package BusinessLogicLayer;

import Manager.PageFactoryManager;
import Pages.GenerateLoremIpsumPage;
import Pages.HomePage;
import Pages.RadioButton;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Assert;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BBL {


    WebDriver driver;
    GenerateLoremIpsumPage generateLoremIpsumPage;
    HomePage homePage;
    RadioButton radioButton;

    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }


    @When("User open russian version of site")
    public void userOpenRussianVersionOfSite() {
        homePage.clickOnRussianLanguageOfSite();
    }

    @Then("User check up that first paragraph contains some {string}")
    public void userCheckUpThatFirstParagraphContainsSomeWord(final String word) {
        Assert.assertTrue(homePage.getTextOfElement().contains(word));
    }


    @Then("User check up that {string} contains some {string}")
    public void userCheckUpThatNumberParagraphContainsSomeWord(final String numberParagraph, final String word) {
        int i = Integer.parseInt(numberParagraph);
        Assert.assertTrue(homePage.getNumberParagraph(i).getText().contains(word));
    }

    @When("User generate Lorem Ipsum")
    public void userGenerateLoremIpsum() {
        homePage.clickToGenerateLoremIpsum();
    }

    @Then("User sees text that start with Lorem Ipsum")
    public void userSeesTextThatStartWithLoremIpsum() {
        generateLoremIpsumPage = pageFactoryManager.getGenerateLoremIpsumPage();
        Assert.assertTrue(generateLoremIpsumPage.isFirstParagraphStartWith());
    }

    @And("User uncheck start with Lorem Ipsum")
    public void userUncheckStartWithLoremIpsum() {
        homePage.clickOnCheckboxStartWith();
    }

    @Then("User sees text that do not start with Lorem Ipsum")
    public void userSeesTextThatDoNotStartWithLoremIpsum() {
        generateLoremIpsumPage = pageFactoryManager.getGenerateLoremIpsumPage();
        Assert.assertFalse(generateLoremIpsumPage.isFirstParagraphStartWith());
    }

    @When("User sets {string}")
    public void userSetsGenerationValue(final String value) {
        radioButton = pageFactoryManager.getRadioButtonPage();
        radioButton.setValue(value);
    }

    @And("User sets {string} of generation value")
    public void userSetsAmountOfGenerationValue(final String amount) {
        homePage.enterAmountOfValue(amount);
    }

    @Then("The user sees that the generation text contains the correct {string} of the given {string}")
    public void theUserSeesThatTheGenerationTextContainsTheCorrectAmountOfTheGivenGenerationValue(final String amount1, final String value1) {
        if (value1 == "words") Assert.assertEquals(generateLoremIpsumPage.theNumberOfWordInParagraph(), amount1);
        if (value1 == "bytes") Assert.assertEquals(generateLoremIpsumPage.getBytesFromText(), amount1);
    }

    @After
    public void tearDown() {
        driver.close();
    }
}

