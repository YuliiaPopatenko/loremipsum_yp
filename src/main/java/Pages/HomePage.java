package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {
    private static final long DEFAULT_TIMEOUT = 60;
    private static final long DEFAULT_TIMEOUT_02 = 20;

    private static final String AMOUNT_OF_WORD = "10";
    private static final String AMOUNT_OF_BYTES = "10";

    @FindBy(xpath = "//a[@href='http://ru.lipsum.com/']")
    private WebElement russianLanguageOfSite;

    @FindBy(xpath = "//div[@id='Panes']//p")
    private WebElement element;

    @FindBy(xpath = "//div[@id='Panes']//p")
    private List<WebElement> listOfParagraphHomePage;

    @FindBy(xpath = "//input[@name='generate']")
    private WebElement generateLI;

    @FindBy(xpath = "//input[@value='words']")
    private WebElement inputWord;

    @FindBy(xpath = "//input[@name='amount']")
    private WebElement inputAmount;

    @FindBy(xpath = "//input[@value='bytes']")
    private WebElement inputBytes;

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement checkboxStartWith;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public WebElement getNumberParagraph(int number) {
        waitVisibilityOfElements(DEFAULT_TIMEOUT_02, listOfParagraphHomePage);
        WebElement numberOfParagraph = listOfParagraphHomePage.get(number);
        return numberOfParagraph;
    }

    public void clickOnRussianLanguageOfSite() {
        russianLanguageOfSite.click();
    }

    public String getTextOfElement() {
        waitVisibilityOfElement(DEFAULT_TIMEOUT_02, element);
        String getText = element.getText();
        return getText;
    }

    public void clickToGenerateLoremIpsum() {
        generateLI.click();

    }

    public void clickOnInputWord() {
        inputWord.click();
    }

    public void enterAmountInInputAmount() {
        inputAmount.clear();
        inputAmount.sendKeys(AMOUNT_OF_WORD);
    }

    public void enterAmountOfValue(String amountOfValue) {
        inputAmount.clear();
        inputAmount.sendKeys(amountOfValue);
    }

    public void clickInputBytes() {
        inputBytes.click();
    }

    public void enterAmountBytes() {
        inputAmount.clear();
        inputAmount.sendKeys(AMOUNT_OF_BYTES);
    }

    public void clickOnCheckboxStartWith() {
        checkboxStartWith.click();
    }
}
