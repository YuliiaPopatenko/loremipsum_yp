package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RadioButton extends BasePage {
    public RadioButton(WebDriver driver) {
        super(driver);
    }

    public void setValue(String value) {
        String xPathForValue = "//label[contains(text(),'" + value + "')]";
        WebElement radioButton = driver.findElement(By.xpath(xPathForValue));
        radioButton.click();
    }
}
