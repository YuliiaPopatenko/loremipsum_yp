package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GenerateLoremIpsumPage extends BasePage {
    private static final long DEFAULT_TIMEOUT = 60;
    private static final long DEFAULT_TIMEOUT_02 = 20;
    private static final String TEXT_START_WITH = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";


    @FindBy(xpath = "//div[@id='lipsum']//p")
    private List<WebElement> listOfParagraph;

    public GenerateLoremIpsumPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getFirstParagraph() {
        waitVisibilityOfElements(DEFAULT_TIMEOUT_02, listOfParagraph);
        WebElement firstParagraph = listOfParagraph.get(0);
        return firstParagraph;
    }

    public String getTextOfFirstParagraph() {
        waitVisibilityOfElement(DEFAULT_TIMEOUT_02, getFirstParagraph());
        String getTextOfParagraph = getFirstParagraph().getText();
        return getTextOfParagraph;
    }

    public boolean isFirstParagraphStartWith() {
        boolean isStart = false;
        if (getTextOfFirstParagraph().startsWith(TEXT_START_WITH)) isStart = true;
        return isStart;
    }

    public int theNumberOfWordInParagraph() {
        int blockCount = getTextOfFirstParagraph().split(" ").length;
        return blockCount;
    }

    public int getBytesFromText() {
        byte[] byteArray = getTextOfFirstParagraph().getBytes();
        int byteCount = byteArray.length;
        return byteCount;
    }

    public int getNumberOfParagraphContainsWordLorem() {

        int quantityLorem = 0;
        for (WebElement webElement : listOfParagraph) {
            waitVisibilityOfElement(DEFAULT_TIMEOUT_02, webElement);
            if (webElement.getText().contains("lorem") || webElement.getText().contains("Lorem"))
                quantityLorem++;
        }
        return quantityLorem;

    }
}
