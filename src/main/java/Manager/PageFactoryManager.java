package Manager;

import Pages.GenerateLoremIpsumPage;
import Pages.HomePage;
import Pages.RadioButton;
import org.openqa.selenium.WebDriver;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public GenerateLoremIpsumPage getGenerateLoremIpsumPage() {
        return new GenerateLoremIpsumPage(driver);
    }

    public RadioButton getRadioButtonPage() {
        return new RadioButton(driver);
    }
}
