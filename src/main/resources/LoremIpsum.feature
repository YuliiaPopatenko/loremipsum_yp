Feature: LoremIpsum functionally

  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check that first paragraph on the home page contains some word
    Given User opens '<homePage>' page
    When User open russian version of site
    Then User check up that '<number paragraph>' contains some '<word>'
    Examples:
      | homePage                | number paragraph | word  |
      | https://www.lipsum.com/ | 0                | рыба  |
      | https://www.lipsum.com/ | 1                | текст |

  Scenario Outline: Check that default setting result in text contains Lorem Ipsum
    Given User opens '<homePage>' page
    When User generate Lorem Ipsum
    Then User sees text that start with Lorem Ipsum
    Examples:
      | homePage                |
      | https://www.lipsum.com/ |

  Scenario Outline: Uncheck start with Lorem Ipsum checkbox
    Given User opens '<homePage>' page
    And User uncheck start with Lorem Ipsum
    When User generate Lorem Ipsum
    Then User sees text that do not start with Lorem Ipsum
    Examples:
      | homePage                |
      | https://www.lipsum.com/ |

  Scenario Outline: Verify that Lorem Ipsum generated with correct size
    Given User opens '<homePage>' page
    When User sets '<generation value>'
    And User sets '<amount>' of generation value
    And User generate Lorem Ipsum
    Then The user sees that the generation text contains the correct '<amount>' of the given '<generation value>'
    Examples:
      | homePage                | generation value | amount |
      | https://www.lipsum.com/ | words            | 10     |
      | https://www.lipsum.com/ | bytes            | 10     |




