package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckTextLoremIpsum extends BaseTest {
    private static final String EXPECTED_KEYWORD = "рыба";

    @Test
    public void checkThatTextContainsWordFish() {
        getHomePage().clickOnRussianLanguageOfSite();
        Assert.assertTrue(getHomePage().getTextOfElement().contains(EXPECTED_KEYWORD));
    }

}
