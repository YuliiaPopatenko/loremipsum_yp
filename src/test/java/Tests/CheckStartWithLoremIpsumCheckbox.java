package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckStartWithLoremIpsumCheckbox extends BaseTest {

    @Test
    public void checkThatDefaultSettingResultInTextStartingWithLoremIpsum() {
        getHomePage().clickToGenerateLoremIpsum();
        Assert.assertTrue(getGenerateLoremIpsumPage().isFirstParagraphStartWith());
    }

    @Test
    public void uncheckStartWithLoremIpsumCheckbox() {
        getHomePage().clickOnCheckboxStartWith();
        getHomePage().clickToGenerateLoremIpsum();
        Assert.assertFalse(getGenerateLoremIpsumPage().isFirstParagraphStartWith());
    }


}
