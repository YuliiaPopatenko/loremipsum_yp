package Tests;

import org.junit.Assert;
import org.testng.annotations.Test;

public class CheckTheProbabilityOfTheWordLoremInTheParagraph extends BaseTest {
    private static final String URL_LOREM_IPSUM = "https://www.lipsum.com/";
    private static final int GENERATE_COUNT = 11;


    @Test
    public void checkTheProbabilityOfTheWordInTheParagraph() {
        int qLorem = 0;
        for (int i = 0; i < GENERATE_COUNT; i++) {
            getDriver().get(URL_LOREM_IPSUM);
            getHomePage().clickToGenerateLoremIpsum();
            qLorem = qLorem + getGenerateLoremIpsumPage().getNumberOfParagraphContainsWordLorem();
        }
        double average = 0;
        average = qLorem / GENERATE_COUNT;
        boolean containsLoremMoreThan40Percent = false;
        if (average >= 2) containsLoremMoreThan40Percent = true;
        Assert.assertTrue(containsLoremMoreThan40Percent);
    }
}

