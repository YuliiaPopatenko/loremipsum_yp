package Tests;

import Pages.GenerateLoremIpsumPage;
import Pages.HomePage;
import Pages.RadioButton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BaseTest {

    private WebDriver driver;
    private static final String LIPSUM_URL = "https://www.lipsum.com/";

    @BeforeTest
    public void profileSetUp() {
        chromedriver().setup();
    }


    @BeforeMethod
    public void testSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(LIPSUM_URL);

    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }


    public WebDriver getDriver() {
        return driver;
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public GenerateLoremIpsumPage getGenerateLoremIpsumPage() {
        return new GenerateLoremIpsumPage(getDriver());
    }

    public RadioButton getRadioButton() {
        return new RadioButton(getDriver());
    }


}



