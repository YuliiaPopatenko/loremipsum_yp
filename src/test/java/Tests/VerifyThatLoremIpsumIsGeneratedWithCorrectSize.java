package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class VerifyThatLoremIpsumIsGeneratedWithCorrectSize extends BaseTest {
    private static final int EXPECTED_QUANTITY_WORDS = 10;
    private static final int EXPECTED_QUANTITY_BYTES = 10;

    @Test
    public void verifyThatLoremIpsumIsGeneratedWithCorrectSize() {
        getHomePage().clickOnInputWord();
        getHomePage().enterAmountInInputAmount();
        getHomePage().clickToGenerateLoremIpsum();
        Assert.assertEquals(getGenerateLoremIpsumPage().theNumberOfWordInParagraph(), EXPECTED_QUANTITY_WORDS);
    }

    @Test
    public void verifyThatLoremIpsumIsGeneratedWithCorrectSizeOfByte() {
        getHomePage().clickInputBytes();
        getHomePage().enterAmountBytes();
        getHomePage().clickToGenerateLoremIpsum();
        Assert.assertEquals(getGenerateLoremIpsumPage().getBytesFromText(), EXPECTED_QUANTITY_BYTES);
    }

}
